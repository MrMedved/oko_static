$(document).ready(function() {

	function AddReportViewModel() {
		var self = this;

		self.percents = ko.observableArray([0, 5, 10, 15, 20, 25, 30, 35, 40,
			45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, ]);
		self.workTypes = ko.observableArray();

		self.generalComment = ko.observable();
		self.uploaders = ko.observableArray();

		self.isSending = false;

		self.submit = function(e) {
			if (self.isSending)
				return false;

			if (ko.validation.group(self, { deep: true })().length>0) {
				ko.validation.group(self, { deep: true }).showAllMessages(true);
			} else {
				var workTypesArr = [];

				self.workTypes().forEach(function(wt) {
					var images = $.fileuploader.getInstance($('#images_'+wt.id())).getFiles();
					var imageIds = [];
					images.forEach(function(el) {
						if (el.imageId) {
							imageIds.push(el.imageId);
						}
					});

					if ( !((wt.percent() == wt.initialPercent()) && wt.comment().length == 0) ) {
						workTypesArr.push(
							{
								"work_type_id": wt.id(),
								"percent": wt.percent(),
								"comment": wt.comment(),
								"image_ids": imageIds,
							}
						)
					}
				});

				var data = {
					general_comment: self.generalComment(),
					work_types_reports: workTypesArr,
				}

				self.isSending = true;
				$.ajax({
					method: 'POST',
					url: '/api/v1/houses/'+house_id+'/reports/',
					contentType: "application/json",
					headers: {
						Accept: "application/json",
					},
					data: JSON.stringify(data)
				}).done(function(result) {
					self.isSending = false;
					if (redirectUrl) {
						window.location.href = redirectUrl;
					}
				});
			}


			return false;
		}

		self.init = function() {
			var workTypesArr = [];
			workTypesData.forEach(function(el) {
				workTypesArr.push( new WorkType(el) );
			});
			self.workTypes(workTypesArr)

			ko.validation.group(self, { deep: true }).showAllMessages(false);
		}
	}

	function WorkType(initData) {
		var self = this;

		self.id = ko.observable(initData.work_type_id);
		self.name = ko.observable(initData.work_type_name);
		self.initialPercent = ko.observable(initData.percent);
		self.percent = ko.observable(initData.percent).extend({
				required: {
					params: true,
					message: 'обязательное поле',
				}
			});
		self.comment = ko.observable('').extend({
			validation: {
				validator: function (val) {
					var isRequired = false;

					if (self.percent() < self.initialPercent())
						isRequired = true;

					var imagesInput = $.fileuploader.getInstance($('#images_'+self.id()));
					var images = imagesInput ? imagesInput.getFiles() : [];
					if (images.length > 0 && self.percent() == self.initialPercent())
						isRequired = true;

					if (isRequired) {
						return val.length > 0;
					}

					return true;
				},
				message: 'Обязательное поле',
			}
		});
	}


	var addReportViewModel = new AddReportViewModel();


	ko.bindingHandlers.dropdown = {
		init: function(element, valueAccessor) {
			$(element).select2();
		},
		update: function(element, valueAccessor) {
			$(element).select2();
		},
	}

	ko.bindingHandlers.fileuploader = {
		init: function(element, valueAccessor) {
			// enable fileuploader plugin
			$(element).fileuploader({
				extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
				changeInput: ' ',
				theme: 'thumbnails',
				enableApi: true,
				addMore: true,
				inputNameBrackets: false,
				thumbnails: {
					box: '<div class="fileuploader-items">' +
							  '<ul class="fileuploader-items-list">' +
								  '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner">+</div></li>' +
							  '</ul>' +
						  '</div>',
					item: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
								   '</div>' +
								   '<div class="progress-holder">${progressBar}</div>' +
							   '</div>' +
						   '</li>',
					item2: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
								   '</div>' +
							   '</div>' +
						   '</li>',
					startImageRenderer: true,
					canvasImage: false,
					_selectors: {
						list: '.fileuploader-items-list',
						item: '.fileuploader-item',
						start: '.fileuploader-action-start',
						retry: '.fileuploader-action-retry',
						remove: '.fileuploader-action-remove'
					},
					onItemShow: function(item, listEl) {
						var plusInput = listEl.find('.fileuploader-thumbnails-input');

						plusInput.insertAfter(item.html);

						if(item.format == 'image') {
							item.html.find('.fileuploader-item-icon').hide();
						}
					}
				},
				afterRender: function(listEl, parentEl, newInputEl, inputEl) {
					var plusInput = listEl.find('.fileuploader-thumbnails-input'),
						api = $.fileuploader.getInstance(inputEl.get(0));

					plusInput.on('click', function() {
						api.open();
					});
				},

				// while using upload option, please set
				// startImageRenderer: false
				// for a better effect
				upload: {
					url: '/api/v1/photos/',
					data: null,
					type: 'POST',
					enctype: 'multipart/form-data',
					start: true,
					synchron: true,
					beforeSend: function(item, listEl, parentEl, newInputEl, inputEl) {
						addReportViewModel.uploaders.push(inputEl[0].getAttribute('name'));

						return true;
					},
					onSuccess: function(data, item, listEl, parentEl, newInputEl, inputEl, textStatus, jqXHR) {
						item.imageId = jqXHR.responseJSON.id ? jqXHR.responseJSON.id : null;
						addReportViewModel.workTypes().forEach(function(wt) {ko.validation.validateObservable(wt.comment)});
						setTimeout(function() {
							item.html.find('.progress-holder').hide();
							item.renderImage();
						}, 400);
					},
					onError: function(item) {
						item.html.find('.progress-holder').hide();
						item.html.find('.fileuploader-item-icon i').text('Failed!');

						setTimeout(function() {
							item.remove();
						}, 1500);
					},
					onProgress: function(data, item) {
						var progressBar = item.html.find('.progress-holder');

						if(progressBar.length > 0) {
							progressBar.show();
							progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
						}
					},

					// Callback fired after all files were uploaded
					onComplete: function(listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus) {
						setTimeout(function(){

						addReportViewModel.uploaders.remove(inputEl[0].getAttribute('name'))
						},3000)
					}

				},

				dragDrop: {
					container: '.fileuploader-thumbnails-input'
				},
				onRemove: function(item) {
					setTimeout(function() {
						addReportViewModel.workTypes().forEach(function(wt) {ko.validation.validateObservable(wt.comment)});
					}, 0);
				}
			});
		},
	}

	ko.validation.init({
		errorElementClass: 'error',
		decorateInputElement: true,
		decorateElementOnModified: true,
	});

	var getCookie = function(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie !== '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) === (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}

	var setCSRFAjaxHeader = function() {
		var csrftoken = getCookie('csrftoken');

		function csrfSafeMethod(method) {
			return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		}

		$.ajaxSetup({
			beforeSend: function(xhr, settings) {
				if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
					xhr.setRequestHeader("X-CSRFToken", csrftoken);
				}
			}
		});

	}

	setCSRFAjaxHeader();

	window.addReportViewModel = addReportViewModel;
	ko.applyBindings(addReportViewModel);
	addReportViewModel.init();
});