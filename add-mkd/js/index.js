$(document).ready(function() {
    function AddMkdViewModel() {
        var self = this;

        self.administrativeDistrict = ko.observable();
        self.municipalDistrict = ko.observable();
        self.street = ko.observable();
        self.streetType = ko.observable();
        self.houseNumber = ko.observable();

        self.activePage = ko.observable(1);
        self.administrativeDistricts = ko.observableArray();
        self.municipalDistricts = ko.observableArray();
        self.streetTypes = ko.observableArray();

        self.selectAdministrativeDistrict = function(item) {
            self.administrativeDistrict(item.name)
            self.activePage(2);
            return false;
        }

        self.selectMunicipalDistrict = function(item) {
            self.municipalDistrict(item.name)
            self.activePage(3);
            return false;
        }

        self.clear = function() {
            self.administrativeDistrict(null);
            self.municipalDistrict(null);
            self.street(null);
            self.streetType(null);
            self.houseNumber(null);
        }

        self.submit = function() {
            console.log('submit', {
                'administrativeDistrict': self.administrativeDistrict(),
                'municipalDistrict': self.municipalDistrict(),
                'street': self.street(),
                'streetType': self.streetType(),
                'houseNumber': self.houseNumber(),
            })
            alert('submit');
            self.clear();
            self.activePage(1);

            return false;
        }

        self.init = function(){
            if ( administrativeDistricts && municipalDistricts && streetTypes ) {
                self.administrativeDistricts(administrativeDistricts);
                self.municipalDistricts(municipalDistricts);
                self.streetTypes(streetTypes);
                console.log('init success');
            } else {
                console.log('init error');
                return;
            }
        }
    }


    ko.bindingHandlers.dropdown = {
        init: function(element, valueAccessor) {
            $(element).select2();
        },
    }


    var addMkdViewModel = new AddMkdViewModel();

    window.addMkdViewModel = addMkdViewModel;
    ko.applyBindings(addMkdViewModel);
    addMkdViewModel.init();
});