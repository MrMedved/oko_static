// for offline testing
NO_API = false;
error_redirect = '/error_page_500'
// NO_API = true;

$(document).ready(function() {
    function MkdRemovationViewModel() {
        var self = this;
        self.house = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });
        self.dueYear = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });
        if (window.scriptMode && window.scriptMode == "edit") {
            self.houses = ko.observableArray();
        } else {
            var firstHouse = new ChildHouse();
            firstHouse.name('-');
            firstHouse.dueYear('-');
            self.houses = ko.observableArray([
                firstHouse
            ]);
        }
        self.isSending = false;

        self.houseNumbers = ko.observableArray([]);
        self.dueYears = ko.observableArray([]);
        self.supervisorContracts = ko.observableArray([]);
        self.workContracts = ko.observableArray([]);
        self.workTypes = ko.observableArray([]);
        self.fkrInspectors = ko.observableArray([]);

        self.redirectUrl = ko.observable();
        self.addMkdModal = ko.observable();
        self.confirmChangesModal = ko.observable();
        self.changesConfirmed = false;

        self.submitIsActive = ko.computed(function() {
            var isActive = true;
            if (ko.validation.group(self, { deep: true }).length > 0) {
                isActive = false;
            }
            return isActive;
        });

        self.openAddMkd = function() {
            if (!self.addMkdModal()) {
                self.addMkdModal($('[data-remodal-id=addMkd]').remodal());
            }
            self.addMkdModal().open();
            addMkdViewModel.clear();

            return false;
        }

        self.openConfrimChanges = function() {
            if (!self.confirmChangesModal()) {
                self.confirmChangesModal($('[data-remodal-id=confirmChanges]').remodal());
            }
            self.confirmChangesModal().open();

            return false;
        }

        self.addChildHouse = function() {
            self.houses.push(new ChildHouse);

            return false;
        }

        self.confirmAndSave = function() {
            self.changesConfirmed = true;
            self.submit();

            return false;
        }

        self.submit = function() {
            if (self.isSending)
                return false;

            var validationResult = ko.validation.group(self, { deep: true });
            if (validationResult().length > 0) {
                validationResult.showAllMessages(true);
            } else {
                self.isSending = true;

                var data = {
                    "parent_house_relation": null,
                    "childs_house_relations": [],
                }

                self.houses().forEach(function(house, index) {
                    var workTypesArr = [];
                    house.workTypes().forEach(function(el) {
                        if (el.from() && el.to()) {
                            if (el.active()) {
                                workTypesArr.push({
                                    "work_type_id": el.id,
                                    "start_date": moment(el.from(), 'DD.MM.YYYY').format('YYYY-MM-DD'),
                                    "end_date": moment(el.to(), 'DD.MM.YYYY').format('YYYY-MM-DD')
                                })
                            }
                        }
                    });

                    if (index == 0) {
                        var houseObj = {
                            "contract_supervisor_id": house.supervisorContract().id,
                            "contract_builder_id": house.workContract().id,
                            "house_id": self.house().id,
                            "work_type_relations": workTypesArr,
                            "repair_year": self.dueYear(),
                            "supervisor": house.supervisorPerson() ? house.supervisorPerson().id : null,
                            "build_installation": house.workPerson() ? house.workPerson().id : null,
                            "fkr_inspector": house.fkrInspector() ? house.fkrInspector().id : null,
                        }
                        if (window.scriptMode && window.scriptMode == "edit") {
                            houseObj.id = house.id();
                        }
                        data.parent_house_relation = houseObj;
                    } else {
                        var houseObj = {
                            "contract_supervisor_id": house.supervisorContract().id,
                            "contract_builder_id": house.workContract().id,
                            "work_type_relations": workTypesArr,
                            "repair_year": self.dueYear(),
                            "name": house.name(),
                            "supervisor": house.supervisorPerson() ? house.supervisorPerson().id : null,
                            "build_installation": house.workPerson() ? house.workPerson().id : null,
                            "fkr_inspector": house.fkrInspector() ? house.fkrInspector().id : null,
                        }
                        if (window.scriptMode && window.scriptMode == "edit") {
                            houseObj.id = house.id();
                        }
                        data.childs_house_relations.push(houseObj);
                    }
                });

                if (window.scriptMode && window.scriptMode == "edit") {
                    if (self.changesConfirmed) {
                        $.ajax({
                            method: 'POST',
                            url: '/api/v1/house_relations/update/',
                            contentType: "application/json",
                            headers: {
                                Accept: "application/json",
                            },
                            data: JSON.stringify(data)
                        }).done(function(result) {
                            self.isSending = false;

                            if (self.redirectUrl()) {
                                window.location.href = self.redirectUrl();
                            }
                        }).fail(function(responseData, textStatus, xhr) {
                            self.isSending = false;
                            Raven.captureMessage('тело запрса:'+JSON.stringify(data)+', \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/house_relations/update/, код ответа:'+responseData.status)
                            setTimeout(function() {
                                window.location.href = error_redirect;
                            }, 1000)
                        });
                    } else {
                        $.ajax({
                            method: 'POST',
                            url: '/api/v1/house_relations/check/',
                            contentType: "application/json",
                            headers: {
                                Accept: "application/json",
                            },
                            data: JSON.stringify(data)
                        }).done(function(result) {
                            self.isSending = false;
                            if (result.status && result.status == 'ok') {
                                self.confirmAndSave();
                            }
                            if (result.status && result.status == "warning") {
                                $('#confirmationText').html(result.content);
                                self.openConfrimChanges();
                            }
                        }).fail(function(responseData, textStatus, xhr) {
                            self.isSending = false;
                            Raven.captureMessage('тело запрса:'+JSON.stringify(data)+', \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/house_relations/check/, код ответа:'+responseData.status)
                            setTimeout(function() {
                                window.location.href = error_redirect;
                            }, 1000)
                        });
                    }
                } else {
                    $.ajax({
                        method: 'POST',
                        url: '/api/v1/house_relations/create/',
                        contentType: "application/json",
                        headers: {
                            Accept: "application/json",
                        },
                        data: JSON.stringify(data)
                    }).done(function(result) {
                        self.isSending = false;
                        if (self.redirectUrl()) {
                            window.location.href = self.redirectUrl();
                        }
                    }).fail(function(responseData, textStatus, xhr) {
                        self.isSending = false;
                        Raven.captureMessage('тело запрса:'+JSON.stringify(data)+', \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/house_relations/create/, код ответа:'+responseData.status)
                        setTimeout(function() {
                            window.location.href = error_redirect;
                        }, 1000)
                    });
                }

            }

            return false;
        }

        self.loadFkrInspectors = function(next) {
            self.fkrInspectors([]);
            $.ajax({
                method: 'GET',
                url: '/api/v1/fkr_users/',
            }).done(function(data) {
                if (data) {
                    self.fkrInspectors(data);
                }
                next()
            }).fail(function(responseData, textStatus, xhr) {
                self.isSending = false;
                Raven.captureMessage('тело запрса:, \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/fkr_users/, код ответа:'+responseData.status)
                setTimeout(function() {
                    window.location.href = error_redirect;
                }, 1000)
            });

            if (NO_API) {
                self.fkrInspectors([{name: 'piotr', id: 'p123'}]);
            }
        }

        self.removeChildHouse = function(childHouse) {
            self.houses.remove(childHouse);
            return false;
        }

        self.init = function() {
            if ( houseNumbers && dueYears && supervisorContracts && workContracts && workTypes && redirectUrl) {
                self.houseNumbers(houseNumbers);
                self.dueYears(dueYears);
                self.supervisorContracts(supervisorContracts);
                self.workContracts(workContracts);
                self.workTypes(workTypes);
                self.redirectUrl(redirectUrl);
                self.loadFkrInspectors(function() {
                    if (window.scriptMode && window.scriptMode == "edit") {
                        var houseObj = {
                            name: editObject.parent_house_relation.house_name,
                            id: editObject.parent_house_relation.house_id,
                        };
                        self.houseNumbers.push(houseObj);
                        self.house(houseObj);

                        self.dueYear(editObject.parent_house_relation.repair_year);

                        var housesArr = [editObject.parent_house_relation].concat( editObject.childs_house_relations);

                        housesArr.forEach(function(parsedHouse, housesIdx) {
                            var parentHouse = new ChildHouse();

                            if (housesIdx>0) {
                                parentHouse.name(parsedHouse.name);
                            } else {
                                parentHouse.name(parsedHouse.house_name);
                            }
                            parentHouse.dueYear(parsedHouse.repair_year);

                            self.supervisorContracts().forEach(function(el) {
                                if (el.id == parsedHouse.contract_supervisor_id) {
                                    parentHouse.supervisorContract(el);
                                }
                            });
                            parentHouse.supervisorPersonProto = parsedHouse.supervisor;

                            self.workContracts().forEach(function(el) {
                                if (el.id == parsedHouse.contract_builder_id) {
                                    parentHouse.workContract(el);
                                }
                            });
                            parentHouse.workPersonProto = parsedHouse.build_installation

                            self.fkrInspectors().forEach(function(el) {
                                if (el.id == parsedHouse.fkr_inspector) {
                                    parentHouse.fkrInspector(el);
                                }
                            });

                            var workTypes = parentHouse.workTypes();
                            workTypes.forEach(function(wt) {
                                parsedHouse.work_type_relations.forEach(function(el) {
                                    if (wt.id == el.work_type_id) {
                                        wt.from(moment(el.start_date, 'YYYY-MM-DD').format('DD.MM.YYYY'));
                                        wt.to(moment(el.end_date, 'YYYY-MM-DD').format('DD.MM.YYYY'));
                                        wt.active(true)
                                    }
                                })
                            });
                            parentHouse.workTypes(workTypes);

                            if (parsedHouse.id) {
                                parentHouse.id(parsedHouse.id);
                            }


                            self.houses.push(parentHouse);
                        });
                    }
                });
            }
        }
    }

    function ChildHouse() {
        var self = this;

        self.id = ko.observable();
        self.name = ko.observable();
        self.dueYear = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });

        // СК
        self.supervisorContract = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });
        self.supervisorPerson = ko.observable();
        self.supervisorPersonProto = null;
        self.supervisorPersons = ko.observableArray([]);
        self.supervisorContract.subscribe(function(newValue){
            if (newValue) {
                self.supervisorPersons([]);
                $.ajax({
                    method: 'GET',
                    url: '/api/v1/supervisor_users/?contract_supervisor_id='+newValue.id,
                }).done(function(data) {
                    if (data) {
                        self.supervisorPersons(data);

                        if (window.scriptMode && window.scriptMode == "edit") {
                            self.supervisorPersons().forEach(function(el) {
                                if (el.id == self.supervisorPersonProto)
                                    self.supervisorPerson(el);
                            });
                        }
                    }
                }).fail(function(responseData, textStatus, xhr) {
                    self.isSending = false;
                    Raven.captureMessage('тело запрса:, \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/supervisor_users/?contract_supervisor_id='+newValue.id+', код ответа:'+responseData.status)
                    setTimeout(function() {
                        window.location.href = error_redirect;
                    }, 1000)
                });

                if (NO_API) {
                    self.supervisorPersons([{name: 'boris', id: 'b123'}]);
                }
            } else {
                self.supervisorPersons([]);
            }
        });

        // СМР
        self.workContract = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });
        self.workPerson = ko.observable();
        self.workPersonProto = null;
        self.workPersons = ko.observableArray();
        self.workContract.subscribe(function(newValue){
            if (newValue) {
                self.workPersons([]);
                $.ajax({
                    method: 'GET',
                    url: '/api/v1/builder_users/?contract_builder_id='+newValue.id,
                }).done(function(data) {
                    if (data) {
                        self.workPersons(data);

                        if (window.scriptMode && window.scriptMode == "edit") {
                            self.workPersons().forEach(function(el) {
                                if (el.id == self.workPersonProto)
                                    self.workPerson(el);
                            });
                        }
                    }
                }).fail(function(responseData, textStatus, xhr) {
                    self.isSending = false;
                    Raven.captureMessage('тело запрса:, \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/builder_users/?contract_builder_id='+newValue.id+', код ответа:'+responseData.status)
                    setTimeout(function() {
                        window.location.href = error_redirect;
                    }, 1000)
                });

                if (NO_API) {
                    self.workPersons([{name: 'piotr', id: 'p123'}]);
                }
            } else {
                self.workPersons([]);
            }
        });

        // ФКР
        self.fkrInspector = ko.observable();

        self.workTypesProto = null;
        self.workTypes = ko.observableArray();
        workTypes.forEach(function(el) {
            self.workTypes.push({
                name: el.name,
                id: el.id,
                from: ko.observable(),
                to: ko.observable(),
                active: ko.observable(window.scriptMode && window.scriptMode == "edit" ? false : true),
            });
        });

        self.toggleWorkType = function(workType) {
            var workTypes = self.workTypes();
            workTypes.forEach(function(el) {
                if (el.id == workType.id) {
                    el.active(!el.active());
                }
            });
            self.workTypes(workTypes);
            return false;
        }
    }

    // add mkd
    function AddMkdViewModel() {
        var self = this;

        self.activePage = ko.observable(1);
        self.administrativeDistricts = ko.observableArray();
        self.municipalities = ko.observableArray();
        self.municipalSubjects = ko.observableArray();
        self.areas = ko.observableArray();
        self.streetTypes = ko.observableArray();
        self.isSending = false;

        self.administrativeDistrict = ko.observable();
        self.municipality = ko.observable();
        self.municipalitySubject = ko.observable();
        self.area = ko.observable().extend({
            validation: {
                validator: function (val) {
                    if ( self.areas().length == 0 ) {
                        return true;
                    }

                    return !!val;
                },
                message: 'Обязательное поле',
            }
        });
        self.street = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });
        self.streetType = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });
        self.houseNumber = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });
        self.mapString = ko.observable().extend({
            required: {
                params: true,
                message: 'Обязательное поле',
            }
        });

        self.selectAdministrativeDistrict = function(item) {
            self.municipalities([]);
            $.ajax({
                method: 'GET',
                url: '/api/v1/municipalities/?administrative_district_id='+item.id,
            }).done(function(data) {
                if (data) {
                    self.municipalities(data);
                }
            }).fail(function(responseData, textStatus, xhr) {
                self.isSending = false;
                Raven.captureMessage('тело запрса:, \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/municipalities/?administrative_district_id='+item.id+', код ответа:'+responseData.status)
                setTimeout(function() {
                    window.location.href = error_redirect;
                }, 1000)
            });

            if (NO_API) {
                self.municipalities(municipalDistricts);
            }

            self.administrativeDistrict(item)
            self.activePage(2);
            return false;
        }

        self.selectMunicipality = function(item) {
            self.municipalSubjects([]);
            $.ajax({
                method: 'GET',
                url: '/api/v1/subject_municipalities/?municipality_id='+item.id,
            }).done(function(data) {
                if (data) {
                    self.municipalSubjects(data);
                }
            }).fail(function(responseData, textStatus, xhr) {
                self.isSending = false;
                Raven.captureMessage('тело запрса:, \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/subject_municipalities/?municipality_id='+item.id+', код ответа:'+responseData.status)
                setTimeout(function() {
                    window.location.href = error_redirect;
                }, 1000)
            });

            if (NO_API) {
                self.municipalSubjects(municipalDistricts);
            }

            self.municipality(item)
            self.activePage(3);
            return false;
        }

        self.selectSubjectMunicipality = function(item) {
            self.areas([]);
            $.ajax({
                method: 'GET',
                url: '/api/v1/areas/?subject_municipality_id='+item.id,
            }).done(function(data) {
                if (data) {
                    self.areas(data);
                }
                self.municipalitySubject(item)
                self.activePage(4);
                setTimeout(function() {
                    ko.validation.group(self, { deep: true }).showAllMessages(false);
                }, 0)
            }).fail(function(responseData, textStatus, xhr) {
                self.isSending = false;
                Raven.captureMessage('тело запрса:, \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/areas/?subject_municipality_id='+item.id+', код ответа:'+responseData.status)
                setTimeout(function() {
                    window.location.href = error_redirect;
                }, 1000)
            });


            if (NO_API) {
                self.areas(municipalDistricts);
            }

            return false;
        }

        self.clear = function() {
            self.administrativeDistrict(null);
            self.municipality(null);
            self.municipalitySubject(null);
            self.area(null);
            self.street(null);
            self.streetType(null);
            self.houseNumber(null);
            self.mapString(null);
            self.activePage(1);
        }

        self.submit = function() {
            if (self.isSending)
                return false;
            self.isSending = true;

            var validationResult = ko.validation.group(self, { deep: true });
            if (validationResult().length > 0) {
                validationResult.showAllMessages(true);
            } else {
                var data = {
                    street: self.street(),
                    street_type_id: self.streetType().id,
                    house: self.houseNumber(),
                    maps_url: self.mapString(),
                    area_id: self.area() && self.area().id,
                    subject_municipality_id: self.municipality().id,
                }

                $.ajax({
                    method: 'POST',
                    url: '/api/v1/houses/create/',
                    data: data
                }).done(function(result) {
                    self.isSending = false;
                    if (result) {
                        var newHouse = {
                            id: result.id,
                            name: result.street+' '+result.house
                        }
                        mkdRemovationViewModel.houseNumbers.push(newHouse);
                        mkdRemovationViewModel.house( newHouse );
                        mkdRemovationViewModel.addMkdModal().close();
                    }

                    self.clear();
                }).fail(function(responseData, textStatus, xhr) {
                    self.isSending = false;
                    if (data.responseJSON) {
                        if (data.responseJSON.maps_url) {
                            addMkdViewModel.mapString.setError(data.responseJSON.maps_url.join(', '))
                        }
                        return
                    }
                    if ( !responseData.status == 400 ) {
                        Raven.captureMessage('тело запрса:'+JSON.stringify(data)+', \n тело ответа:'+responseData.responseText+', урл запрса: /api/v1/houses/create/, код ответа:'+responseData.status)
                        setTimeout(function() {
                            window.location.href = error_redirect;
                        }, 1000)
                    }
                });

                if (NO_API) {
                    var newHouse = {
                        id: 'somenewid',
                        name: 'sesame st, 1'
                    }
                    mkdRemovationViewModel.houseNumbers.push(newHouse)
                    mkdRemovationViewModel.house( newHouse );
                    mkdRemovationViewModel.addMkdModal().close();
                }
            }

            return false;
        }

        self.init = function(){
            if ( administrativeDistricts && streetTypes ) {
                self.administrativeDistricts(administrativeDistricts);
                self.streetTypes(streetTypes);
            } else {
                return;
            }
        }
    }
    // end add mkd

    ko.bindingHandlers.dropdown = {
        init: function(element, valueAccessor) {
            $(element).select2();
        },
        update: function(element, valueAccessor) {
            $(element).select2();
        },
    }

    ko.bindingHandlers.datepicker = {
        init: function(element, valueAccessor) {
            valueAccessor()
            $(element).pikaday({format: 'D.MM.YYYY'});
        },
        update: function(element, valueAccessor) {
            valueAccessor()
            $(element).pikaday({format: 'D.MM.YYYY'});
        },
    }


    var mkdRemovationViewModel = new MkdRemovationViewModel();
    var addMkdViewModel = new AddMkdViewModel();

    window.mkdRemovationViewModel = mkdRemovationViewModel;
    window.addMkdViewModel = addMkdViewModel;
    ko.validation.init({
       errorElementClass: 'error',
       decorateInputElement: true,
       decorateElementOnModified: true,
    })
    ko.applyBindings(mkdRemovationViewModel);
    mkdRemovationViewModel.init();
    addMkdViewModel.init();

    var getCookie = function(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var setCSRFAjaxHeader = function() {
        var csrftoken = getCookie('csrftoken');

        function csrfSafeMethod(method) {
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

    }

    setCSRFAjaxHeader();
});
